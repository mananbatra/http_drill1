const { error } = require('console');
const http= require('http');
const fs=require('fs');
const path=require('path');
const uuid=require('crypto');
const {port}=require('./config.js')

const htmlFilePath=path.join(__dirname,'./htmlFile.html');
const jsonFilePath=path.join(__dirname,'./jsonFile.json')


let server =http.createServer((request,response) =>{

    if(request.method==="GET" && request.url.toLowerCase()==="/html"){

        response.setHeader("Content-Type","text/html")
        fs.readFile(htmlFilePath,'utf8',(error,data) =>{
            if(error)
            {
                console.error(error);
            }else{
                response.end(data)
            }
        })
        
    }
    else if(request.method==="GET" && request.url.toLowerCase()==="/json"){

        response.setHeader("Content-Type","application/json")
        fs.readFile(jsonFilePath,'utf8',(error,data) =>{
            if(error)
            {
                console.error(error);
            }else{
                response.end(data)
            }
        })
    }
    else if(request.method==="GET" && request.url.toLowerCase()==="/uuid"){

        response.setHeader("Content-Type","application/json");

        const uuidV4=uuid.randomUUID()
        response.end(
            `{ "uuid": ${uuidV4} }`
        )
    }
    else if(request.method==="GET" && request.url.toLowerCase().startsWith('/status')){

        response.setHeader("Content-Type","application/json")

        const statusID = Number(request.url.split('/')[2]);
        //console.log(status);
        if (!http.STATUS_CODES[statusID]|| statusID===100) {
            return response.end(`{Bad Request: 400, Enter A Valid Status Code:!! }`)
        }
        response.statusCode = statusID;
        response.end(`Your response is with :  ${statusID}`);
    }
    else if(request.method==="GET" && request.url.toLowerCase().startsWith('/delay')){
        
        response.setHeader("Content-Type","application/json")

        const time = request.url.split('/').pop()
        if (time >= 0 && isNaN(time) === false) {
            setTimeout(() => {
                response.end(JSON.stringify({"Page Loaded after " :  + time + " seconds."}));
                }, time * 1000);                                                                                          //defined delay in seconds
        }
        else {
            response.end(JSON.stringify({"Error" : "Try entering positive numeric values to show delayed output "}));
        }

    }
    else{
        response.end(`<!DOCTYPE html>
    <html>
      <head><title>Welcome to My WebPage</title>
      </head>
      <body>
          <h1>Access Different Feature using given links </h1>
          <ol>
          <li> - <a href="https://manan-batra-http-drill.onrender.com/html">HTML File</a></li>
          <li> - <a href="https://manan-batra-http-drill.onrender.com/json">JSON File</a></li>
          <li> - <a href="https://manan-batra-http-drill.onrender.com/uuid">Random uuid</a></li>
          <li> - Add <a href="https://manan-batra-http-drill.onrender.com/status/200">"/status/{status-code}"</a>to your URL to get status codes</li>
          <li> - Add <a href="https://manan-batra-http-drill.onrender.com/delay/2">"/delay/{delay_in_seconds}"</a> to your URL to get delay in page loading</li>
          </ol><br>
          <marquee>Thanks for having a presence on my web page</marquee>
          <marquee>Web Page created by Manan Batra</marquee>
      </body>
    </html>`)

    }
})
.listen(port,(error) =>{

    if(error)
    {
        console.error(error)
    }else{

        console.log("server is running")
    }
})